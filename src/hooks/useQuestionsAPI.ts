import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { QuestionActions } from '../shared/enum';
import { Constants } from '../shared/constants';

const useQuestionsAPI = (axiosParams: AxiosRequestConfig, toDispatch: (arg0: any) => {type: QuestionActions; response: any}) => {
  const dispatch = useDispatch();
  const [response, setResponse] = useState<AxiosResponse>();
  const [error, setError] = useState<AxiosError>();
  const [loading, setLoading] = useState(true);
  let isUnique:boolean = false;
  // this needs just for seafty rason to avoid infinite loops 
  // e.x.: if you set the questionsAmount to 50+ we will never have 50+ unique questions (this api has only 50 questions in hard category)
  let loopCounter:number = 0;
  const setUnique = () => { isUnique = true }
  const incrementCounter = () => { loopCounter++ }

  const fetchData = async (params: AxiosRequestConfig) => {
    try {
      //it would be a generic axios api hook if this 'unique' think wouldnt be a thing. imho: the questions should be unique by default when i query them /it is until the amount <= 50/
      while (!isUnique && loopCounter < 5) {
        await axios.request(params).then((response:AxiosResponse) => {
          const result:Array<string> = response.data.results.map((item:any) => {
            return item.question;
          });
          const unique:Array<string> = [...Array.from(new Set(result))];
          if(unique.length === Constants.questionsAmount){
            // ahh ESlint...
            setUnique();
          }
          incrementCounter();
          dispatch(toDispatch(response));
          setResponse(response);
        });
      }

      } catch( err:any ) {
        setError(err);
      } finally {
        setLoading(false);
      }
      
 };

  useEffect(() => {
    fetchData(axiosParams);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[axiosParams.url]);


  return {response, error, loading}
}

export default useQuestionsAPI