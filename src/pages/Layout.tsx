import { Container } from "react-bootstrap";
import { Outlet } from "react-router-dom";

const Layout = () => {
  return (
    <Container>
        <div className="justify-content-md-center vh-100">
            <Outlet />
        </div>
    </Container>
    
  )
}

export default Layout