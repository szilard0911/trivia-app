import { Button,  Col, Row, Spinner } from "react-bootstrap"
import { Constants } from "../shared/constants"
import { MouseEvent, useState } from 'react';
import { useNavigate } from "react-router-dom";
import { useDispatch } from 'react-redux';

import "./Quiz.scss";
import useQuestionsAPI from '../hooks/useQuestionsAPI';
import { Question } from "../shared/interfaces";
import { setQuestionsAction, setQuestionAnswerAction} from '../shared/store/questions.actions';
import { decode } from "html-entities";


const Quiz = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  // Call the API: Pass the AxiosRequestConfig object and the Action funtion has to be dispatched on success 
  const questionsAmount:number = Constants.questionsAmount;
  const difficulty:string = Constants.difficulty;
  const {response, loading, error} = useQuestionsAPI({
    method: "GET",
    baseURL: Constants.APIBaseURL,
    url: `api.php?amount=${questionsAmount}&difficulty=${difficulty}&type=boolean`,
    headers: {
      accept: 'application/json'
    }
  }, setQuestionsAction);

  // Map response and dispatch setQuestionsAction
  const questionsResponse:Question[] = response?.data.results.map((item:any) => { 
    return{
      category: item.category,
      correct_answer: item.correct_answer === "True" ? true : false,
      difficulty: item.difficulty,
      question: item.question
    }
  });
 
  // init questionIndex
  const [questionIndex, setQuestionIndex] = useState<number>(0);
  

  // Handle onClick event of the answer buttons
  const onClickAnswer = (event:MouseEvent<HTMLButtonElement>, userAnswer:boolean) => {
    event.preventDefault();
    if(response){
      const question:Question = {
        ...questionsResponse[questionIndex],
        userAnswer: userAnswer
      };

      //save the answer
      dispatch(setQuestionAnswerAction(question));

      // increment quesionIndex
      if(questionIndex + 1 < questionsResponse.length){
        setQuestionIndex(questionIndex + 1);
      }else{
        navigate('/results');
      }
    }
  }

  // display loading screen
  if (loading) {
    return (
      <div className="position-absolute top-50 start-50 translate-middle">
        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </div>
    );
  }
  // display Error if occured
  if (error) {
    return (
      <div className="position-absolute top-50 start-50 translate-middle">
        <div>
          <span >Error has occured: {error.name} - {error.message} <br />Error code: {error.code}</span>
        </div>
      </div>
    );
  }
  // display the curent question (after quetions are loaded and no error)
  return (
    <div className="text-center d-flex flex-column align-items-center vh-100 quiz-container">
      <div className="mb-auto">
        <h2 className="py-3">{questionsResponse[questionIndex].category}</h2>
      </div>
      <div className="border border-secondary border-3 d-flex align-items-center quiz-container__question-card">
        <p className="text-center fs-4 m-3 text-wrap">{ decode(questionsResponse[questionIndex].question) }</p>
      </div>
      <div>
        <p className="fs-8 py-3">{questionIndex + 1} of {questionsAmount}</p>
      </div>
      <Row className="my-5 mt-auto" >
        <Col>
          <Button 
            variant="outline-secondary"
            onClick={(event) => onClickAnswer(event, false)}
          >FALSE</Button>
        </Col>
        <Col>
        <Button 
            variant="outline-secondary"
            onClick={(event) => onClickAnswer(event, true)}
          >TRUE</Button>
        </Col>
      </Row>
      
    </div>
  )
}

export default Quiz