import { decode } from "html-entities";
import { Button, Row, Stack, Col } from "react-bootstrap";
import { BsDashLg, BsPlusLg } from "react-icons/bs";
import { MouseEvent } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Question } from "../shared/interfaces";
import { State } from "../shared/store/state";

const Results = () => {
  const { questions } = useSelector((state:State) => state);
  const navigate = useNavigate();
  let rightAnswersCount:number = 0;

  questions.forEach((question) => {
    if(question.userAnswer){
      if(question.userAnswer === question.correct_answer) rightAnswersCount++;
    }
    
  })

  // Handle onClick event of the BEGIN button
  const onClickPlayAgain = (event:MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    navigate('/quiz');
  }

  return (
    // <div>
    //   <div>You scored</div>
    //   <div>{rightAnswersCount}/{questions.length}</div>
    //   <div>{questions.map((question:Question, index:number) => {
    //     return <p key={index}>{decode(question.question)}</p>
    //   })}</div>
    // </div>

    <div className="d-flex flex-column align-items-center min-vh-100 pb-3 ">
      <div className="mb-auto">
        <h2 className="py-3 text-center">You scored<br />{rightAnswersCount}/{questions.length}</h2>
      </div>
      <Stack gap={2}>
        {questions.map((question:Question, index:number) => {
          return (<Row  className="justify-content-md-center text-secondary" key={index}>
            <Col className="text-center" xs={1}>{question.correct_answer === question.userAnswer ? <BsPlusLg /> : <BsDashLg /> }</Col>
            <Col ><h4>{decode(question.question)}</h4></Col>
          </Row>);
        })}
      </Stack>
      <Button 
        className="my-3" 
        variant="outline-secondary"
        onClick={onClickPlayAgain}
      >PLAY AGAIN</Button>
    </div>
  )
}

export default Results