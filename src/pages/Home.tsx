import { MouseEvent } from "react";
import { Button } from "react-bootstrap"
import { useNavigate } from "react-router-dom";
import { Constants } from "../shared/constants";

const Home = () => {
  const questionsAmount:number = Constants.questionsAmount; 
  const navigate = useNavigate();

  // Handle onClick event of the BEGIN button
  const onClickBegin = (event:MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    navigate('/quiz');
  }

  // Flex solution
  return (

    <div className="text-center d-flex flex-column align-items-center vh-100 ">
      <div className="mb-auto">
        <h2 className="py-3">Welcome to the Trivia Chellenge</h2>
      </div>
      <div>
        <p className="fs-3 py-3">You will be presented with {questionsAmount} True or False questions.</p>
        <p className="fs-3 py-3">Can you score 100%?</p>
      </div>
      <Button 
        className="my-3 mt-auto" 
        variant="outline-secondary"
        onClick={onClickBegin}
      >BEGIN</Button>
    </div>
  )

  // position-absolute solution
  /* return (
    <div className="text-center">
      <div className="position-absolute top-0 start-50 translate-middle-x">
        <h2 className="py-3">Welcome to the Trivia Chellenge</h2>
      </div>
      <div className="position-absolute top-50 start-50 translate-middle ">
        <p className="fs-3 py-3">You will be presented with 10 True or False questions.</p>
        <p className="fs-3 py-3">Can you score 100%?</p>
      </div>
      <Button 
        className="position-absolute bottom-0 start-50 translate-middle-x my-3" 
        variant="outline-secondary"
      >BEGIN</Button>
    </div>
  ) */

}

export default Home