import {
  BrowserRouter as Router,
  Routes,
  Route
} from 'react-router-dom'

import './App.scss';
//Import pages
import Home from './pages/Home';
import Layout from './pages/Layout';
import NoMatch from './pages/NoMatch';
import Quiz from './pages/Quiz';
import Results from './pages/Results';

const App = () => {
  return (
    <Router>
      <Routes>
        <Route path='/' element={<Layout />}>
          <Route index element={<Home />} />  
          <Route path='/quiz' element={<Quiz />} />
          <Route path='/results' element={<Results />} />
          <Route path="*" element={<NoMatch />} />
        </Route>
      </Routes>
    </Router>
  );
}

export default App;
