export enum QuestionActions {
  SetQuestionAnswer_Success = 'SET_QUESTION_ANSWER_SUCCESS',
  SetQuestions_Success = 'SET_QUESTIONS_SUCCESS',
  INIT = '@@INIT'
}