export interface Question {
  category: string;
  correct_answer: boolean;
  difficulty: string;
  question: string;
  userAnswer?: boolean;
}