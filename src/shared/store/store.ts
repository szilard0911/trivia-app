import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { InitialState } from './initialState';
import { reducer } from "./questions.reducer";

const middleware = [thunk];

export default createStore(reducer, InitialState, composeWithDevTools(applyMiddleware(...middleware)));
