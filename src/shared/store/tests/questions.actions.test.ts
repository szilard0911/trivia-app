import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import { questions, axiosResponse } from "./__fixtures__/questions";
import { Question } from '../../interfaces';

import {
  setQuestionAnswerAction,
  setQuestionsAction
} from "./../questions.actions";

const createMockStore = configureMockStore([thunk]);

test("should setup set questions action object with data", () => {
  const action = setQuestionsAction(axiosResponse);
  expect(action).toEqual({
    type: "SET_QUESTIONS_SUCCESS",
    response: axiosResponse
  });
});

test('should set the first action to set questions with data', () => {
  const store = createMockStore({
    questions: []
  });
  store.dispatch(setQuestionsAction(axiosResponse));
  const actions = store.getActions();
  expect(actions[0]).toEqual({
    type: "SET_QUESTIONS_SUCCESS",
    response: axiosResponse
  });
});

test("should setup set question answer action object with data", () => {
  const answeredQuestion:Question = {
    category: "Entertainment: Video Games",
    correct_answer: false,
    difficulty: "hard",
    question:
      "In the game &quot;Melty Blood Actress Again Current Code&quot;, you can enter Blood Heat mode in Half Moon style.",
    userAnswer: false
  }
  const action = setQuestionAnswerAction(answeredQuestion);
  expect(action).toEqual({
    type: "SET_QUESTION_ANSWER_SUCCESS",
    response: answeredQuestion
  });
});

test('should set the first action to set question answer with data', () => {
  const store = createMockStore({
    questions: questions
  });
  const answeredQuestion:Question = {
    category: "Entertainment: Video Games",
    correct_answer: false,
    difficulty: "hard",
    question:
      "In the game &quot;Melty Blood Actress Again Current Code&quot;, you can enter Blood Heat mode in Half Moon style.",
    userAnswer: false
  }
  store.dispatch(setQuestionAnswerAction(answeredQuestion));
  const actions = store.getActions();
  expect(actions[0]).toEqual({
    type: "SET_QUESTION_ANSWER_SUCCESS",
    response: answeredQuestion
  });
});

