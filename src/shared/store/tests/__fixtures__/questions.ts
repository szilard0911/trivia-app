import { AxiosResponse } from "axios";
import { Question } from "../../../interfaces"

export const questions: Question[] = [
  {
    category: 'Entertainment: Video Games',
    correct_answer: false,
    difficulty: 'hard',
    question: 'In the game &quot;Melty Blood Actress Again Current Code&quot;, you can enter Blood Heat mode in Half Moon style.'
  },
  {
    category: 'Science & Nature',
    correct_answer: true,
    difficulty: 'hard',
    question: 'It was once believed that injecting shark cartilage into people would prevent them from contracting cancer.'
  },
  {
    category: 'Science: Computers',
    correct_answer: false,
    difficulty: 'hard',
    question: 'DHCP stands for Dynamic Host Configuration Port.'
  },
  {
    category: 'History',
    correct_answer: false,
    difficulty: 'hard',
    question: 'The Battle of Trafalgar took place on October 23rd, 1805'
  },
  {
    category: 'Geography',
    correct_answer: false,
    difficulty: 'hard',
    question: 'Switzerland has four national languages, English being one of them.'
  },
  {
    category: 'General Knowledge',
    correct_answer: true,
    difficulty: 'hard',
    question: 'Stagecoach owned &quot;South West Trains&quot; before losing the rights to FirstGroup and MTR in March of 2017.'
  },
  {
    category: 'Entertainment: Film',
    correct_answer: false,
    difficulty: 'hard',
    question: 'The weapon Clint Eastwood uses in &quot;Dirty Harry&quot; was a .44 Automag.'
  },
  {
    category: 'Entertainment: Music',
    correct_answer: true,
    difficulty: 'hard',
    question: 'The song Scatman&#039;s World was released after Scatman (Ski-Ba-Bop-Ba-Dop-Bop).'
  },
  {
    category: 'Geography',
    correct_answer: true,
    difficulty: 'hard',
    question: 'Only one country in the world starts with the letter Q.'
  },
  {
    category: 'Entertainment: Video Games',
    correct_answer: true,
    difficulty: 'hard',
    question: 'In The Witcher 3, the Zoltan Chivay Gwent card can be found under the Hanged Man&#039;s Tree.'
  }
];

export const axiosResponse: AxiosResponse<any, any> = {
  data: {
    response_code: 0,
    results: [
      {
        category: "Entertainment: Video Games",
        type: "boolean",
        difficulty: "hard",
        question:
          "In the game &quot;Melty Blood Actress Again Current Code&quot;, you can enter Blood Heat mode in Half Moon style.",
        correct_answer: "False",
        incorrect_answers: ["True"]
      },
      {
        category: "Science & Nature",
        type: "boolean",
        difficulty: "hard",
        question:
          "It was once believed that injecting shark cartilage into people would prevent them from contracting cancer.",
        correct_answer: "True",
        incorrect_answers: ["False"]
      },
      {
        category: "Science: Computers",
        type: "boolean",
        difficulty: "hard",
        question: "DHCP stands for Dynamic Host Configuration Port.",
        correct_answer: "False",
        incorrect_answers: ["True"]
      },
      {
        category: "History",
        type: "boolean",
        difficulty: "hard",
        question: "The Battle of Trafalgar took place on October 23rd, 1805",
        correct_answer: "False",
        incorrect_answers: ["True"]
      },
      {
        category: "Geography",
        type: "boolean",
        difficulty: "hard",
        question:
          "Switzerland has four national languages, English being one of them.",
        correct_answer: "False",
        incorrect_answers: ["True"]
      },
      {
        category: "General Knowledge",
        type: "boolean",
        difficulty: "hard",
        question:
          "Stagecoach owned &quot;South West Trains&quot; before losing the rights to FirstGroup and MTR in March of 2017.",
        correct_answer: "True",
        incorrect_answers: ["False"]
      },
      {
        category: "Entertainment: Film",
        type: "boolean",
        difficulty: "hard",
        question:
          "The weapon Clint Eastwood uses in &quot;Dirty Harry&quot; was a .44 Automag.",
        correct_answer: "False",
        incorrect_answers: ["True"]
      },
      {
        category: "Entertainment: Music",
        type: "boolean",
        difficulty: "hard",
        question:
          "The song Scatman&#039;s World was released after Scatman (Ski-Ba-Bop-Ba-Dop-Bop).",
        correct_answer: "True",
        incorrect_answers: ["False"]
      },
      {
        category: "Geography",
        type: "boolean",
        difficulty: "hard",
        question: "Only one country in the world starts with the letter Q.",
        correct_answer: "True",
        incorrect_answers: ["False"]
      },
      {
        category: "Entertainment: Video Games",
        type: "boolean",
        difficulty: "hard",
        question:
          "In The Witcher 3, the Zoltan Chivay Gwent card can be found under the Hanged Man&#039;s Tree.",
        correct_answer: "True",
        incorrect_answers: ["False"]
      }
    ]
  },
  status: 200,
  statusText: "",
  headers: {
    "cache-control": "no-store, no-cache, must-revalidate",
    "content-type": "application/json",
    expires: "Thu, 19 Nov 1981 08:52:00 GMT",
    pragma: "no-cache"
  },
  config: {
    transitional: {
      silentJSONParsing: true,
      forcedJSONParsing: true,
      clarifyTimeoutError: false
    },
    transformRequest: [],
    transformResponse: [],
    timeout: 0,
    xsrfCookieName: "XSRF-TOKEN",
    xsrfHeaderName: "X-XSRF-TOKEN",
    maxContentLength: -1,
    maxBodyLength: -1,
    headers: {
      Accept: "application/json"
    },
    method: "get",
    baseURL: "https://opentdb.com/",
    url: "api.php?amount=10&difficulty=hard&type=boolean"
  },
  request: {}
};
