import { questions, axiosResponse } from "./__fixtures__/questions";
import { reducer } from "../questions.reducer";
import { QuestionActions } from "../../enum";

test("should setup default qustions values", () => {
  const state = reducer(undefined, {
    type: QuestionActions.INIT
  });
  expect(state).toEqual({
    questions: []
  });
});

test("should setup 10 questions values", () => {
  const state = reducer(undefined, {
    type: QuestionActions.SetQuestions_Success,
    response: axiosResponse
  });
  expect(state).toEqual({
    questions: questions
  });
});

test("should answer a question", () => {
  const state = reducer({questions: questions}, {
    type: QuestionActions.SetQuestionAnswer_Success,
    response: {
      category: "Entertainment: Video Games",
      correct_answer: false,
      difficulty: "hard",
      question:
        "In the game &quot;Melty Blood Actress Again Current Code&quot;, you can enter Blood Heat mode in Half Moon style.",
      userAnswer: false
    }
  });
  expect(state.questions[0].userAnswer).toBe(false);

});
