import { QuestionActions } from "../enum";
import { Question } from "../interfaces"
import { AxiosResponse } from 'axios';

// Create Action Types
export type SetQuestionAnswerAction = {
    type: QuestionActions.SetQuestionAnswer_Success,
    response: Question
};

export type SetQuestionsAction = {
    type: QuestionActions.SetQuestions_Success,
    response: AxiosResponse
};
// This one needs only for the unit tests
export type INITAction = {
    type: QuestionActions.INIT
};

//Concatenate the Action Types into one Type
export type QuestionAction = SetQuestionAnswerAction | SetQuestionsAction | INITAction;

//Actions
export const setQuestionAnswerAction = (response: Question) => ({
    type: QuestionActions.SetQuestionAnswer_Success,
    response 
});

export const setQuestionsAction = (response: AxiosResponse) => ({
    type: QuestionActions.SetQuestions_Success,
    response 
});