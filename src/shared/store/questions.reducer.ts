import { InitialState } from './initialState';
import { State } from './state';
import { QuestionAction } from './questions.actions';
import { QuestionActions } from '../enum';
import { Question } from '../interfaces';

export const reducer = (state:State = InitialState,  action:QuestionAction) => {
  switch (action.type) {
    case QuestionActions.SetQuestionAnswer_Success:
      return {
        ...state,
        questions: state.questions.map((question) => { 
          if(question.question === action.response.question){
            return action.response;
          }
          return question;
         })
      }
    case QuestionActions.SetQuestions_Success:

      let questions:Question[] = action.response.data.results?.map((item:any) => { 
        return{
          category: item.category,
          correct_answer: item.correct_answer === "True" ? true : false,
          difficulty: item.difficulty,
          question: item.question
        }
      });
      return {
        ...state,
        questions: questions
      }
    default:
      return state;
  }
}