import { Question } from "../interfaces";
/* export namespace State {
    export type Questions = Question[];
    export type CurrentQuestion = Question;
} */

export type State = {
    questions: Question[]
}
