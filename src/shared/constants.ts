export class Constants {
    public static readonly questionsAmount:number = 10;
    public static readonly difficulty:string = 'hard';
    public static readonly APIBaseURL:string = 'https://opentdb.com/';

}