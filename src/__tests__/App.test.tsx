import React from 'react';
import { render, screen } from '@testing-library/react';
import App from '../App';

test('renders BEGIN button', () => {
  render(<App />);
  const beginButton = screen.getByText(/BEGIN/i);
  expect(beginButton).toBeInTheDocument();
});
